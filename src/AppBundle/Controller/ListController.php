<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\YoutubeType;
use Google_Client;
use Google_Service_YouTube;

class ListController extends Controller
{
    const DEVELOPER_KEY = 'REPLACEME';
    /**
     * @Route("/", name="welcome")
     */
    public function showAction(Request $request)
    {
        $form = $this->createForm(YoutubeType::class);
        $form->handleRequest($request);
        $videos = [];
        if ($form->isSubmitted()) {
            $keyword = $request->request->get('search_string');
            $videos = $this->getVideosByKeyword($keyword);

        }

        return $this->render('youtube/youtube.html.twig', [
            'form' => $form->createView(),
            'videos' => $videos
        ]);
    }

    protected function getVideosByKeyword($keyword) {

        $client = new Google_Client();
        $client->setDeveloperKey(self::DEVELOPER_KEY);

        $youtube = new Google_Service_YouTube($client);

        // Define an object that will be used to make all API requests.
        $searchResponse = $youtube->search->listSearch('id,snippet', array(
            'q' => $keyword,
            'maxResults' => 10,
        ));

        foreach ($searchResponse['items'] as $searchResult) {
            if ($searchResult['id']['kind'] == 'youtube#video') {
                $videoList[] = $searchResult;
            }
        }

        return $videoList;
    }
}